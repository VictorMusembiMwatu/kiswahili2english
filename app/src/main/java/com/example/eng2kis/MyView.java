package com.example.eng2kis;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
class MyView extends RecyclerView.ViewHolder {

    private TextView display_area;
    MyView(@NonNull View itemView) {
        super(itemView);

        display_area = itemView.findViewById(R.id.translated);
    }

    TextView getDisplay_area() {
        return display_area;
    }
}
