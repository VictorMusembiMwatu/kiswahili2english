package com.example.eng2kis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> kiswahili, english;
    private EditText input;
    private TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        kiswahili = new ArrayList<>();
        english    = new ArrayList<>();

        Button exitBtn = findViewById(R.id.exit);
        Button translateBtn = findViewById(R.id.translate);

        input = findViewById(R.id.input);
        output       = findViewById(R.id.output);

        readingTheData();
        translateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                translate();
            }
        });
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });


    }

    private void readingTheData(){
        InputStream  inputStream = getResources().openRawResource(R.raw.words);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null){
               try {
                   String[]maneno = line.split(",");

                   if (maneno.length == 2) {
                       kiswahili.add(maneno[0]);
                       english.add(maneno[1]);
                   }


               }catch (Exception e){
                   Log.e("Error", e.toString());
               }
            }

        }catch (IOException io){
            Log.e("Error", io.toString());
        }
    }

    private void translate(){

        String neno =  input.getText().toString().trim();

        String translationmsg = null;

        ArrayList<String> maneno = new ArrayList<>();

        if (english.contains(neno)){

            String translationsg = "maneno " + neno;

            for (int i = 0; i < english.size(); i++) {
                if (english.get(i).equals(neno))
                    maneno.add(kiswahili.get(i));

            }
        }else if(kiswahili.contains(neno)) {
            translationmsg = "The translations is "+neno;
            for (int i = 0; i < kiswahili.size(); i++) {
                if (kiswahili.get(i).equals(neno))
                    maneno.add(english.get(i));
            }
        }else {
            translationmsg = "Not found";
        }

        output.setText(translationmsg);

        Addapter adapter = new Addapter(maneno, this);
        RecyclerView view = findViewById(R.id.recyclerView);
        view.setAdapter(adapter);
        view.setLayoutManager(new LinearLayoutManager(this));

    }
}
