package com.example.eng2kis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Addapter extends RecyclerView.Adapter<MyView> {


    private ArrayList<String> translations;
    private Context context;


    Addapter(ArrayList<String> words, Context context) {
        this.translations = words;
        this.context = context;
    }

    @NonNull
    @Override
    public MyView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(this.context);

        View view = inflater.inflate(R.layout.maneno, parent, false);
        return new MyView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyView holder, int position) {
        holder.getDisplay_area().setText(translations.get(position));
    }

    @Override
    public int getItemCount() { return translations.size(); }
}
